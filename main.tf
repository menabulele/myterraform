resource "aws_vpc" "main" {
  cidr_block                       = var.vpc_cidr
  instance_tenancy                 = "default"
  enable_dns_hostnames             = true
  enable_dns_support                 = true
  assign_generated_ipv6_cidr_block = true

  tags = {
    Name = "a4l-vpc1"
  }
}

data "aws_availability_zones" "all" {}


resource "aws_subnet" "reserved-a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_reserved-a_cidr
  availability_zone = data.aws_availability_zones.all.names[0]


  tags = {
    Name = "sn-reserved-A"
  }
}

resource "aws_subnet" "reserved-b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_reserved-b_cidr
  availability_zone = data.aws_availability_zones.all.names[1]


  tags = {
    Name = "sn-reserved-B"
  }
}

resource "aws_subnet" "reserved-c" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_reserved-c_cidr
  availability_zone = data.aws_availability_zones.all.names[2]


  tags = {
    Name = "sn-reserved-C"
  }
}

resource "aws_subnet" "db-a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_db-a_cidr
  availability_zone = data.aws_availability_zones.all.names[0]


  tags = {
    Name = "sn-db-A"
  }
}

resource "aws_subnet" "db-b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_db-b_cidr
  availability_zone = data.aws_availability_zones.all.names[1]


  tags = {
    Name = "sn-db-B"
  }
}

resource "aws_subnet" "db-c" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_db-c_cidr
  availability_zone = data.aws_availability_zones.all.names[2]


  tags = {
    Name = "sn-db-C"
  }
}

resource "aws_subnet" "app-a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_app-a_cidr
  availability_zone = data.aws_availability_zones.all.names[0]


  tags = {
    Name = "sn-app-A"
  }
}

resource "aws_subnet" "app-b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_app-b_cidr
  availability_zone = data.aws_availability_zones.all.names[1]


  tags = {
    Name = "sn-app-B"
  }
}

resource "aws_subnet" "app-c" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_app-c_cidr
  availability_zone = data.aws_availability_zones.all.names[2]


  tags = {
    Name = "sn-app-C"
  }
}

resource "aws_subnet" "web-a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_web-a_cidr
  availability_zone = data.aws_availability_zones.all.names[0]


  tags = {
    Name = "sn-web-A"
  }
}

resource "aws_subnet" "web-b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_web-b_cidr
  availability_zone = data.aws_availability_zones.all.names[1]


  tags = {
    Name = "sn-web-B"
  }
}

resource "aws_subnet" "web-c" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.subnet_web-c_cidr
  availability_zone = data.aws_availability_zones.all.names[2]


  tags = {
    Name = "sn-web-B"
  }
}

resource "aws_internet_gateway" "a4l-vpc-igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "a4l-vpc-igw"
  }
}

resource "aws_route_table" "a4l-vpc1-rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.a4l-vpc-igw.id
  }

  tags = {
    Name : "a4l-vpc1-rt"
  }
}

resource "aws_route_table_association" "web-a" {
  subnet_id      = aws_subnet.web-a.id
  route_table_id = aws_route_table.a4l-vpc1-rt.id
}

resource "aws_route_table_association" "web-b" {
  subnet_id      = aws_subnet.web-b.id
  route_table_id = aws_route_table.a4l-vpc1-rt.id
}

resource "aws_route_table_association" "web-c" {
  subnet_id      = aws_subnet.web-c.id
  route_table_id = aws_route_table.a4l-vpc1-rt.id
}

resource "aws_security_group" "A4L-SG" {
  name   = "A4L-SG"
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "A4L-SG"
  }

  # outbound allow all
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  #inbound for ssh
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

  #ELB inbound rule
  ingress {
      from_port = var.elb_port
      to_port = var.elb_port
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
}


#db security group

resource "aws_security_group" "A4L-SG-DB" {
  name   = "A4L-DB-SG"
  vpc_id = aws_vpc.main.id


  #Allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Inbound for SSH
  ingress {
    from_port       = 3306
    protocol        = "tcp"
    to_port         = 3306
    security_groups = [aws_security_group.A4L-SG.id]
  }
}



#bastion web server

resource "aws_instance" "bastion-web" {
  ami                         = var.amiid
  instance_type               = var.type
  key_name                    = "devopsmaster"
  availability_zone           = data.aws_availability_zones.all.names[0]
  vpc_security_group_ids      = [aws_security_group.A4L-SG.id]
  subnet_id                   = aws_subnet.web-a.id
  associate_public_ip_address = true

  tags = {
    Name = "a4l-Bastion-web"
  }


}

resource "aws_instance" "db-instance" {
  ami                         = var.amiid
  instance_type               = var.type
  key_name                    = "devopsmaster"
  availability_zone           = data.aws_availability_zones.all.names[0]
  vpc_security_group_ids      = [aws_security_group.A4L-SG-DB.id]
  subnet_id                   = aws_subnet.db-a.id
  associate_public_ip_address = false

  tags = {
    Name = "a4l-db-instance"
  }
}





resource "aws_instance" "app-instance" {
  ami                         = var.amiid
  instance_type               = var.type
  key_name                    = "devopsmaster"
  availability_zone           = data.aws_availability_zones.all.names[0]
  vpc_security_group_ids      = [aws_security_group.A4L-SG-DB.id]
  subnet_id                   = aws_subnet.db-a.id
  associate_public_ip_address = false

  tags = {
    Name = "a4l-app-instance"
  }
}
